#include "testnumbertotextconverter.h"

void TestNumberToTextConverter::testConvert_data()
{
    QTest::addColumn<quint64>("number");
    QTest::addColumn<LocalExpectations>("expectations");

    LanguageTestsGroup tests =
    {
        {
            "Minimum",
            NAE(Q_UINT64_C(0), LocalExpectations
            {
                {"English", "zero"},
                {"Russian", "ноль"}
            })
        },
        {
            "7",
            NAE(Q_UINT64_C(7), LocalExpectations
            {
                {"English", "seven"},
                {"Russian", "семь"}
            })
        },
        {
            "19",
            NAE(Q_UINT64_C(19), LocalExpectations
            {
                {"English", "nineteen"},
                {"Russian", "девятнадцать"}
            })
        },
        {
            "26",
            NAE(Q_UINT64_C(26), LocalExpectations
            {
                {"English", "twenty six"},
                {"Russian", "двадцать шесть"}
            })
        },
        {
            "55",
            NAE(Q_UINT64_C(55), LocalExpectations
            {
                {"English", "fifty five"},
                {"Russian", "пятьдесят пять"}
            })
        },
        {
            "90",
            NAE(Q_UINT64_C(90), LocalExpectations
            {
                {"English", "ninety"},
                {"Russian", "девяносто"}
            })
        },
        {
            "100",
            NAE(Q_UINT64_C(100), LocalExpectations
            {
                {"English", "one hundred"},
                {"Russian", "сто"}
            })
        },
        {
            "103",
            NAE(Q_UINT64_C(103), LocalExpectations
            {
                {"English", "one hundred and three"},
                {"Russian", "сто три"}
            })
        },
        {
            "123",
            NAE(Q_UINT64_C(123), LocalExpectations
            {
                {"English", "one hundred and twenty three"},
                {"Russian", "сто двадцать три"}
            })
        },
        {
            "130",
            NAE(Q_UINT64_C(130), LocalExpectations
            {
                {"English", "one hundred and thirty"},
                {"Russian", "сто тридцать"}
            })
        },
        {
            "555",
            NAE(Q_UINT64_C(555), LocalExpectations
            {
                {"English", "five hundred and fifty five"},
                {"Russian", "пятьсот пятьдесят пять"}
            })
        },
        {
            "999",
            NAE(Q_UINT64_C(999), LocalExpectations
            {
                {"English", "nine hundred and ninety nine"},
                {"Russian", "девятьсот девяносто девять"}
            })
        },
        {
            "1 000",
            NAE(Q_UINT64_C(1000), LocalExpectations
            {
                {"English", "one thousand"},
                {"Russian", "одна тысяча"}
            })
        },
        {
            "2 000",
            NAE(Q_UINT64_C(2000), LocalExpectations
            {
                {"English", "two thousand"},
                {"Russian", "две тысячи"}
            })
        },
        {
            "5 000",
            NAE(Q_UINT64_C(5000), LocalExpectations
            {
                {"English", "five thousand"},
                {"Russian", "пять тысяч"}
            })
        },
        {
            "25 000",
            NAE(Q_UINT64_C(25000), LocalExpectations
            {
                {"English", "twenty five thousand"},
                {"Russian", "двадцать пять тысяч"}
            })
        },
        {
            "991 000",
            NAE(Q_UINT64_C(991000), LocalExpectations
            {
                {"English", "nine hundred and ninety one thousand"},
                {"Russian", "девятьсот девяносто одна тысяча"}
            })
        },
        {
            "999 000",
            NAE(Q_UINT64_C(999000), LocalExpectations
            {
                {"English", "nine hundred and ninety nine thousand"},
                {"Russian", "девятьсот девяносто девять тысяч"}
            })
        },
        {
            "6941",
            NAE(Q_UINT64_C(6941), LocalExpectations
            {
                {"English", "six thousand, nine hundred and forty one"},
                {"Russian", "шесть тысяч девятьсот сорок один"}
            })
        },
        {
            "Ten thousand",
            NAE(Q_UINT64_C(10000), LocalExpectations
            {
                {"English", "ten thousand"},
                {"Russian", "десять тысяч"}
            })
        },
        {
            "744 000",
            NAE(Q_UINT64_C(744000), LocalExpectations
            {
                {"English", "seven hundred and forty four thousand"},
                {"Russian", "семьсот сорок четыре тысячи"}
            })
        },
        {
            "One million",
            NAE(Q_UINT64_C(1000000), LocalExpectations
            {
                {"English", "one million"},
                {"Russian", "один миллион"}
            })
        },
        {
            "6 743 466",
            NAE(Q_UINT64_C(6743466), LocalExpectations
            {
                {"English", "six million, seven hundred and forty three thousand, four hundred and sixty six"},
                {"Russian", "шесть миллионов семьсот сорок три тысячи четыреста шестьдесят шесть"}
            })
        },
        {
            "3 020",
            NAE(Q_UINT64_C(3020), LocalExpectations
            {
                {"English", "three thousand and twenty"},
                {"Russian", "три тысячи двадцать"}
            })
        },
        {
            "744 000 000",
            NAE(Q_UINT64_C(744000000), LocalExpectations
            {
                {"English", "seven hundred and forty four million"},
                {"Russian", "семьсот сорок четыре миллиона"}
            })
        },
        {
            "One trillion",
            NAE(Q_UINT64_C(1000000000000), LocalExpectations
            {
                {"English", "one trillion"},
                {"Russian", "один триллион"}
            })
        },
        {
            "One quadrillion",
            NAE(Q_UINT64_C(1000000000000000), LocalExpectations
            {
                {"English", "one quadrillion"},
                {"Russian", "один квадриллион"}
            })
        },
        {
            "300 787 111 000 123 876",
            NAE(Q_UINT64_C(300787111000123876), LocalExpectations
            {
                {"English", "three hundred quadrillion, seven hundred and eighty seven trillion, one hundred and eleven billion, one hundred and twenty three thousand, eight hundred and seventy six"},
                {"Russian", "триста квадриллионов семьсот восемьдесят семь триллионов сто одиннадцать миллиардов сто двадцать три тысячи восемьсот семьдесят шесть"}
            })
        },
        {
            "One quintillion",
            NAE(Q_UINT64_C(1000000000000000000), LocalExpectations
            {
                {"English", "one quintillion"},
                {"Russian", "один квинтиллион"}
            })
        },
        {
            "64 bite limit",
            NAE(Q_UINT64_C(18446744073709551615), LocalExpectations
            {
                {"English", "eighteen quintillion, four hundred and forty six quadrillion, seven hundred and forty four trillion, seventy three billion, seven hundred and nine million, five hundred and fifty one thousand, six hundred and fifteen"},
                {"Russian", "восемнадцать квинтиллионов четыреста сорок шесть квадриллионов семьсот сорок четыре триллиона семьдесят три миллиарда семьсот девять миллионов пятьсот пятьдесят одна тысяча шестьсот пятнадцать"}
            })
        }
    };

    for (auto test : tests)
    {
        NumberAndExpectation value = test.second;
        QString description = test.first;
        QTest::newRow(description.toStdString().c_str())
            << value.first
            << value.second;
    }
}

void TestNumberToTextConverter::testConvert()
{
    #ifdef Q_OS_WIN32
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("IBM 866"));
    #endif

    QFETCH(quint64, number);
    QFETCH(LocalExpectations, expectations);

    for (auto expectation : expectations)
    {
        NumberToTextConverter converter;
        QString result = converter.convert(number, expectations.key(expectation));

        QString message = QString("\nExpectation:\n%1\nReal:\n%2\n").arg(expectation).arg(result);

        QVERIFY2(result == expectation, message.toLocal8Bit().data());
    }
}
