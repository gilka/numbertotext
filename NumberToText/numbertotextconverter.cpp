#include "numbertotextconverter.h"

NumberToTextConverter::NumberToTextConverter()
{
    this->converters.insert("English", new NumberToEnglishTextConverter);
    this->converters.insert("Russian", new NumberToRussianTextConverter);
}

NumberToTextConverter::~NumberToTextConverter()
{

}

QString NumberToTextConverter::convert(quint64 number, const QString &language)
{
    INumberToLocalText *converter = converters[language];
    return converter->convert(number);
}