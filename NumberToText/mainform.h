#ifndef MAINFORM_H
#define MAINFORM_H

#include <QtWidgets/QMainWindow>
#include "ui_mainform.h"
#include "numbersvalidator.h"
#include "numbertotextconverter.h"

class MainForm : public QMainWindow
{
    Q_OBJECT

public:
    MainForm(QWidget *parent = 0);
    ~MainForm();

private slots:
    void enableButton();
    void convert();

private:
    Ui::MainFormClass ui;
    NumbersValidator *numbersValidator;
};

#endif // MAINFORM_H
