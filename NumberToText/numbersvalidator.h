#ifndef NUMBERSVALIDATOR_H
#define NUMBERSVALIDATOR_H

#include <QValidator>

class NumbersValidator : public QValidator
{
    Q_OBJECT

public:
    NumbersValidator(QObject *parent);
    ~NumbersValidator();
    
    virtual State validate(QString &input, int &pos) const;
};

#endif // NUMBERSVALIDATOR_H
