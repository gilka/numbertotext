#include "numbertorussiantextconverter.h"

NumberToRussianTextConverter::NumberToRussianTextConverter()
{
    zero = "ноль";

    const char *digitsMasculineArray[] =
    {
        "один", "два", "три", "четыре", "пять",
        "шесть", "семь", "восемь", "девять", NULL
    };

    const char *digitsFeminineArray[] =
    {
        "одна", "две", "три", "четыре", "пять",
        "шесть", "семь", "восемь", "девять", NULL
    };

    QStringList digitsMasculineList;
    QStringList digitsFeminineList;

    for (int i = 0; digitsMasculineArray[i]; ++i)
    {
        digitsMasculineList << digitsMasculineArray[i];
    }

    for (int i = 0; digitsFeminineArray[i]; ++i)
    {
        digitsFeminineList << digitsFeminineArray[i];
    }

    digits.push_back(digitsMasculineList);
    digits.push_back(digitsFeminineList);

    const char *teensArray[] =
    {
        "десять", "одиннадцать", "двенадцать", "тринадцать",
        "четырнадцать", "пятнадцать", "шестнадцать",
        "семнадцать", "восемнадцать", "девятнадцать", NULL
    };

    for (int i = 0; teensArray[i]; ++i)
    {
        teens << teensArray[i];
    }

    const char *tensArray[] =
    {
        "двадцать", "тридцать", "сорок", "пятьдесят",
        "шестьдесят", "семьдесят", "восемьдесят", "девяносто", NULL
    };

    for (int i = 0; tensArray[i]; ++i)
    {
        tens << tensArray[i];
    }

    const char *hundredArray[] =
    {
        "сто", "двести", "триста", "четыреста", "пятьсот",
        "шестьсот", "семьсот", "восемьсот", "девятьсот", NULL
    };

    for (int i = 0; hundredArray[i]; ++i)
    {
        hundred << hundredArray[i];
    }

    const char *scaleSingleNominativeArray[] =
    {
        "тысяча", "миллион", "миллиард", "триллион",
        "квадриллион", "квинтиллион", NULL
    };
    QStringList scaleSingleNominativeList;
    for (int i = 0; scaleSingleNominativeArray[i]; ++i)
    {
        scaleSingleNominativeList << scaleSingleNominativeArray[i];
    }

    const char *scaleSingleGenitiveArray[] =
    {
        "тысячи", "миллиона", "миллиарда", "триллиона",
        "квадриллиона", "квинтиллиона", NULL
    };
    QStringList scaleSingleGenitiveList;
    for (int i = 0; scaleSingleGenitiveArray[i]; ++i)
    {
        scaleSingleGenitiveList << scaleSingleGenitiveArray[i];
    }

    const char *scaleMultipleNominativeArray[] =
    {
        "тысячи", "миллионы", "миллиарды", "триллионы",
        "квадриллионы", "квинтиллионы", NULL
    };
    QStringList scaleMultipleNominativeList;
    for (int i = 0; scaleMultipleNominativeArray[i]; ++i)
    {
        scaleMultipleNominativeList << scaleMultipleNominativeArray[i];
    }

    const char *scaleMultipleGenitiveArray[] =
    {
        "тысяч", "миллионов", "миллиардов", "триллионов",
        "квадриллионов", "квинтиллионов", NULL
    };
    QStringList scaleMultipleGenitiveList;
    for (int i = 0; scaleMultipleGenitiveArray[i]; ++i)
    {
        scaleMultipleGenitiveList << scaleMultipleGenitiveArray[i];
    }

    QList<QStringList> scaleSingleList;
    scaleSingleList.push_back(scaleSingleNominativeList);
    scaleSingleList.push_back(scaleSingleGenitiveList);

    QList<QStringList> scaleMultipleList;
    scaleMultipleList.push_back(scaleMultipleNominativeList);
    scaleMultipleList.push_back(scaleMultipleGenitiveList);

    scale.push_back(scaleSingleList);
    scale.push_back(scaleMultipleList);
}

NumberToRussianTextConverter::~NumberToRussianTextConverter()
{

}

QString NumberToRussianTextConverter::convert(quint64 number)
{
    if (number == 0)
    {
        return zero;
    }

    QVector<int> threeDigitGroups;
    quint64 clone = number;

    int threeDigitGroupCount = DIGITS_COUNT / 3;
    if (threeDigitGroupCount * 3 < DIGITS_COUNT)
    {
        ++threeDigitGroupCount;
    }

    for (int i = 0; i < threeDigitGroupCount; ++i)
    {
        threeDigitGroups << clone % 1000;
        clone /= 1000;
    }

    QStringList groupsText;
    for (int i = 0; i < threeDigitGroupCount; ++i)
    {
        if (i == 1)
        {
            groupsText << convertThreeDigitGroup(threeDigitGroups[i], FEMININE);
        }
        else
        {
            groupsText << convertThreeDigitGroup(threeDigitGroups[i]);
        }
    }

    QString result = groupsText[0];

    for (int i = 1; i < threeDigitGroupCount; ++i)
    {
        if (threeDigitGroups[i] != 0)
        {
            Form form;
            Case wordCase;

            int hundredDigit = threeDigitGroups[i] / 100;
            int tensUnit = threeDigitGroups[i] % 100;
            int tensDigit = tensUnit / 10;
            int unitsDigit = tensUnit % 10;

            if (unitsDigit == 1)
            {
                form = SINGLE;
                wordCase = NOMINATIVE;
            }

            if (unitsDigit >= 2 && unitsDigit <= 4)
            {
                if (i == 1)
                {
                    form = PLURAL;
                    wordCase = NOMINATIVE;
                }
                else
                {
                    form = SINGLE;
                    wordCase = GENITIVE;
                }
            }

            if (unitsDigit == 0 || unitsDigit >= 5)
            {
                form = PLURAL;
                wordCase = GENITIVE;
            }

            if (tensUnit >= 10 && tensUnit <= 19)
            {
                form = PLURAL;
                wordCase = GENITIVE;
            }

            QString groupText = groupsText[i] + " " + scale[(int) form][(int) wordCase][i - 1];
            if (result.length() != 0)
            {
                result = groupText + " " + result;
            }
            else
            {
                result += groupText;
            }
        }
    }

    return result;
}

QString NumberToRussianTextConverter::convertThreeDigitGroup(int threeDigit, const Gender &gender /*= MASCULINE*/)
{
    QString result;
    int hundredDigit = threeDigit / 100;
    int tensUnit = threeDigit % 100;
    int tensDigit = tensUnit / 10;
    int unitsDigit = tensUnit % 10;

    if (hundredDigit != 0)
    {
        result += hundred[hundredDigit - 1];

        if (tensUnit != 0)
        {
            result += " ";
        }
    }

    if (tensDigit >= 2)
    {
        result += tens[tensDigit - 2];

        if (unitsDigit != 0)
        {
            result += " " + digits[(int) gender][unitsDigit - 1];
        }
    }
    else if (tensUnit != 0)
    {
        if (tensUnit < 10)
        {
            result += digits[(int) gender][tensUnit - 1];
        }
        else
        {
            result += teens[tensUnit - 10];
        }
    }

    return result;
}
