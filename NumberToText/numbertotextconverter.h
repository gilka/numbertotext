#ifndef NUMBERTOTEXTCONVERTER_H
#define NUMBERTOTEXTCONVERTER_H

#include <QString>
#include <QMap>
#include "numbertoenglishtextconverter.h"
#include "numbertorussiantextconverter.h"

/*
 * Фабрика конвертеров в определенный язык.
 */
class NumberToTextConverter
{
public:
    NumberToTextConverter();
    ~NumberToTextConverter();

    /*!
     * Конвертирует число в текст на определенном языке.
     *\param[in] number Число для конвертации.
     *\param[in] language Язык результата конвертации.
     *\return Текст, соответствующий числу number, на языке language.
     */
    QString convert(quint64 number, const QString &language);

private:
    quint64 number;
    QMap<QString, INumberToLocalText *> converters;
};

#endif // NUMBERTOTEXTCONVERTER_H
