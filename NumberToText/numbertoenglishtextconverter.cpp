#include "numbertoenglishtextconverter.h"

NumberToEnglishTextConverter::NumberToEnglishTextConverter()
{
    const char *beforeTwentyList[] =
    {
        "zero", "one", "two", "three", "four", "five", "six",
        "seven", "eight", "nine", "ten", "eleven", "twelve",
        "thirteen", "fourteen", "fifteen", "sixteen",
        "seventeen", "eighteen", "nineteen", NULL
    };

    for (int i = 0; beforeTwentyList[i]; ++i)
    {
        beforeTwenty << beforeTwentyList[i];
    }

    const char *tensList[] =
    {
        "twenty", "thirty", "forty", "fifty",
        "sixty", "seventy", "eighty", "ninety", NULL
    };

    for (int i = 0; tensList[i]; ++i)
    {
        tens << tensList[i];
    }

    const char *scaleList[] =
    {
        "thousand", "million", "billion",
        "trillion", "quadrillion", "quintillion", NULL
    };

    for (int i = 0; scaleList[i]; ++i)
    {
        scale << scaleList[i];
    }
}

NumberToEnglishTextConverter::~NumberToEnglishTextConverter()
{

}

QString NumberToEnglishTextConverter::convert(quint64 number)
{
    QString result;

    if (number == 0)
    {
        return beforeTwenty[number];
    }

    QVector<int> threeDigitGroups;
    quint64 clone = number;

    int threeDigitGroupCount = DIGITS_COUNT / 3;
    if (threeDigitGroupCount * 3 < DIGITS_COUNT)
    {
        ++threeDigitGroupCount;
    }

    for (int i = 0; i < threeDigitGroupCount; ++i)
    {
        threeDigitGroups << clone % 1000;
        clone /= 1000;
    }

    QStringList groupsText;
    for (int i = 0; i < threeDigitGroupCount; ++i)
    {
        groupsText << convertThreeDigitGroup(threeDigitGroups[i]);
    }

    result = groupsText[0];
    bool appendAnd = threeDigitGroups[0] > 0 && threeDigitGroups[0] < 100;

    for (int i = 1; i < threeDigitGroupCount; ++i)
    {
        if (threeDigitGroups[i] != 0)
        {
            QString prefix = groupsText[i] + " " + scale[i - 1];
            if (result.length() != 0)
            {
                prefix += appendAnd ? " and " : ", ";
            }

            appendAnd = false;
            result = prefix + result;
        }
    }

    return result;
}

QString NumberToEnglishTextConverter::convertThreeDigitGroup(int threeDigit)
{
    QString result;
    int hundredDigit = threeDigit / 100;
    int tensUnit = threeDigit % 100;
    int tensDigit = tensUnit / 10;
    int unitsDigit = tensUnit % 10;

    if (hundredDigit != 0)
    {
        result += beforeTwenty[hundredDigit] + " hundred";

        if (tensUnit != 0)
        {
            result += " and ";
        }
    }

    if (tensDigit >= 2)
    {
        result += tens[tensDigit - 2];

        if (unitsDigit != 0)
        {
            result += " " + beforeTwenty[unitsDigit];
        }
    }
    else if (tensUnit != 0)
    {
        result += beforeTwenty[tensUnit];
    }

    return result;
}
