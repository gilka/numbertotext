#include "mainform.h"

MainForm::MainForm(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

    ui.convert->setEnabled(false);

    connect(ui.numbers, SIGNAL(textEdited(const QString &)), this, SLOT(enableButton()));
    connect(ui.convert, SIGNAL(clicked()), this, SLOT(convert()));

    numbersValidator = new NumbersValidator(this);
    ui.numbers->setValidator(numbersValidator);
}

MainForm::~MainForm()
{
    delete numbersValidator;
}

void MainForm::enableButton()
{
    if (ui.numbers->hasAcceptableInput())
    {
        ui.convert->setEnabled(true);
    }
    else
    {
        ui.convert->setEnabled(false);
    }
}

void MainForm::convert()
{
    QString result;
    QStringList numbers = ui.numbers->text().split(' ', QString::SkipEmptyParts);

    QString language;
    ui.lang->currentText() == "Английский" ?
        language = "English" :
        language = "Russian";

    int length = numbers.length();
    for (int i = 0; i < length; ++i)
    {
        QString stringNumber = numbers[i];

        bool ok;
        quint64 number = stringNumber.toULongLong(&ok);

        if (!ok)
        {
            throw "Ошибка преобразования строки в число";
        }

        NumberToTextConverter converter;
        result += converter.convert(number, language) + "\n";
    }

    ui.result->setText(result);
}
