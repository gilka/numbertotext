#ifndef NUMBERTOENGTEXT_H
#define NUMBERTOENGTEXT_H

#include <QStringList>
#include <QVector>
#include "inumbertolocaltext.h"

/*
 * Класс для конвертации числа в текст на английском языке.
 */
class NumberToEnglishTextConverter : public INumberToLocalText
{
public:
    /* Создает объект конвертера */
    NumberToEnglishTextConverter();
    ~NumberToEnglishTextConverter();

    /*
    * Конвертирует число в текст на английском языке.
    *\param[in] number Число для конвертации.
    */
    QString convert(quint64 number);

private:
    /*
    * Конвертирует число, принадлежащее (0;999].
    *\param[in] threeDigit Число для конвертации.
    */
    QString convertThreeDigitGroup(int threeDigit);

    /* Числа от 0 до 19. */
    QStringList beforeTwenty;

    /* Числа 20, 30, ..., 90. */
    QStringList tens;

    /* Числа 10^3, 10^6, ..., 10^18. */
    QStringList scale;
};

#endif // NUMBERTOENGTEXT_H
