#include "numbersvalidator.h"

NumbersValidator::NumbersValidator(QObject *parent)
    : QValidator(parent)
{

}

NumbersValidator::~NumbersValidator()
{

}

QValidator::State NumbersValidator::validate(QString &input, int &pos) const
{
    // Ввод должен начинаться с цифры.
    if (input[0] == ' ')
    {
        return Invalid;
    }

    // Поле не должно быть пустым.
    if (input.length() == 0)
    {
        return Intermediate;
    }

    // Можно вводить только цифры и пробелы.
    if (input.contains(QRegularExpression("[^0-9 ]")))
    {
        return Invalid;
    }

    QStringList numbers = input.split(' ', QString::SkipEmptyParts);
    int count = numbers.count();

    // Максимальное количество чисел - 100.
    // Можно поменять, не проблема.
    if (count > 100)
    {
        return Invalid;
    }


    int length = numbers.length();
    for (int i = 0; i < length; ++i)
    {
        QString stringNumber = numbers[i];

        bool ok;
        quint64 number = stringNumber.toULongLong(&ok);

        if (!ok)
        {
            throw "Ошибка преобразования строки в число";
        }

        // Верхняя граница вводимого числа.
        // Нижняя - ноль.
        quint64 limit = 1000000000000000000L;

        if (number > limit)
        {
            return Invalid;
        }
    }

    return Acceptable;
}
