#ifndef NUMBERTORUSSIANTEXTCONVERTER_H
#define NUMBERTORUSSIANTEXTCONVERTER_H

#include <QStringList>
#include <QVector>
#include <QList>
#include "inumbertolocaltext.h"

enum Gender
{
    MASCULINE,
    FEMININE
};

enum Form
{
    SINGLE,
    PLURAL
};

enum Case
{
    NOMINATIVE,
    GENITIVE
};

/*
 * Класс для конвертации числа в текст на русском языке.
 */
class NumberToRussianTextConverter : public INumberToLocalText
{
public:
    /* Создает объект конвертера */
    NumberToRussianTextConverter();
    ~NumberToRussianTextConverter();

    /*
    * Конвертирует число в текст на русском языке.
    *\param[in] number Число для конвертации.
    */
    QString convert(quint64 number);

private:
    /*
    * Конвертирует число, принадлежащее (0;999].
    *\param[in] threeDigit Число для конвертации.
    *\param[in] gender Род, в котором нужно вернуть результат. По умолчанию род мужской.
    */
    QString convertThreeDigitGroup(int threeDigit, const Gender &gender = MASCULINE);

    /* Ноль. */
    QString zero;

    /* Числа, принадлежащие [1;9]. В мужском и женском родах. */
    QList<QStringList> digits;

    /* Числа, принадлежащие [10;19]. */
    QStringList teens;

    /* Числа 20, 30, ..., 90. */
    QStringList tens;

    /* Числа 100, 200, ..., 900. */
    QStringList hundred;

    typedef QList<QStringList> LSL;

    /* 
     * Числа 10^3, 10^6, ..., 10^18.
     * В единственном и множественном числах, именительном и родительном падежах.
     */
    QList<LSL> scale;
};

#endif // NUMBERTORUSSIANTEXTCONVERTER_H
