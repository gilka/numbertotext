#ifndef INUMBERTOTEXTLOCAL_H
#define INUMBERTOTEXTLOCAL_H

#include <QString>

/*
 * Интерфейс конвертации числа в текст на определенном языке.
 */
class INumberToLocalText
{
public:
    virtual ~INumberToLocalText();

    /*
     * Конвертирует число в текст на определенном языке.
     *\param[in] number Число для конвертации.
     */
    virtual QString convert(quint64 number) = 0;

protected:
    /* Максимальная длина 64-битного числа в цифрах (unsigned long long) */
    static const int DIGITS_COUNT;
};

#endif // INUMBERTOTEXTLOCAL_H
